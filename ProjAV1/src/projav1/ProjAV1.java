
package projav1;

import Classe.Dados;
import Classe.Pessoa;
import java.util.ArrayList;
import java.util.Scanner;

public class ProjAV1 {

    public static void main(String[] args) {
       
        Scanner teclado = new Scanner(System.in);
        
        Dados dados = new Dados();
        
        Pessoa p1 = new Pessoa(1,"Joao",10);
        Pessoa p2 = new Pessoa(2,"Alice",5);
        Pessoa p3 = new Pessoa(3,"Fernando",27);
        Pessoa p4 = new Pessoa(4,"Carlos",12);
        Pessoa p5 = new Pessoa(5,"Priscila",31);
        
        dados.cadastrarPessoa(p1);
        dados.cadastrarPessoa(p2);
        dados.cadastrarPessoa(p3);
        dados.cadastrarPessoa(p4);
        dados.cadastrarPessoa(p5);
        
        dados.listarPorIdade();
        
        System.out.println("Digite nome para buscar: ");
        String nome = teclado.next();
        
        dados.buscarNome(nome);
        
        System.out.println("Digite ID: ");
        int id = teclado.nextInt();
        
        dados.consultarDados(id);
               
    }
    
}
