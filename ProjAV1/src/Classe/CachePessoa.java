package Classe;

public class CachePessoa {

private int id;
    private String nome;
    private int idade;

    public CachePessoa(int id, String nome, int idade) {
        this.id = id;
        this.nome = nome;
        this.idade = idade;
    }

    public CachePessoa() {
    }

    public CachePessoa(String nome) {
        this.nome = nome;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }


    @Override
    public String toString() {
        return  "Nome: " + nome + " Idade: " + idade;
    }
    
    public int compareTo(Object obj){
        int valor = 0;
        Pessoa p = (Pessoa) obj;
        
        if(this.idade > p.idade)
            valor = 1;
        else if (this.idade < p.idade)
            valor = -1;
        
        return valor;        
    }
    
    
    
}