
package Classe;

import java.util.ArrayList;
import java.util.Collections;

public class Dados {
    
    ArrayList<Pessoa> pessoas = new ArrayList<>();
    ArrayList<CachePessoa> cp = new ArrayList<>();
    
    public void cadastrarPessoa(Pessoa pessoa){
        pessoas.add(pessoa);
    }
    
    public void listarPorIdade(){
        Collections.sort(pessoas);
        for(Pessoa p: pessoas)
            System.out.println(p.toString());
    }
    
    public void buscarNome(String nome){          
        for(Pessoa pessoa: pessoas){
            if(pessoa.getNome().equals(nome))
                System.out.println(pessoa.toString());
        }
    }
    
    public void consultarDados(int id){
        
        for(CachePessoa c : cp){
            if(id != c.getId()){
                System.out.println("CACHE"+c.toString());
            }else{
                for(Pessoa pessoa: pessoas){
                    if(id == pessoa.getId()){
                        System.out.println(pessoa.toString());
                        cp.add(new CachePessoa(pessoa.getId(), pessoa.getNome(), pessoa.getIdade()));
                    }
                }
            }
        }
        
    }
    
}
